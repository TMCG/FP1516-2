#Projecto de Fundamentos de Programacao - Picross 
#Grupo AL059 
#83430 - Andre Xavier Luis Pereira Lima
#83567 - Tiago Miguel Calhanas Goncalves


#---------------------------------------------------------------
# COORDENADAS
# AUXILIARES
def int_pos(arg):
    """ |int_pos: arg ----> logico
    
|int_pos(arg) recebe um argumento e devolve True se este for um inteiro
maior que 0 e False caso contrario.
    """
    return isinstance(arg, int) and arg > 0


# CONSTRUTOR        
def cria_coordenada(l, c):
    """ |cria_coordenada: inteiro x inteiro ----> coordenada
    
|cria_coordenada(l,c) recebe dois numeros inteiros e caso os argumentos 
sejam validos devolve uma coordenada em que os argumentos correpondem 'a 
linha e coluna, respetivamente.
    """    
    if int_pos(l) and int_pos(c):
        return (l, c)
    else:
        raise ValueError ('cria_coordenada: argumentos invalidos')


# SELETORES
def coordenada_linha(coord):
    """ |coordenada_linha:  coordenada ----> inteiro
    
|coordenada_linha(coord) recebe uma coordenada como argumennto e devolve
um inteiro correspondente 'a linha dessa coordenada.
    """
    return coord[0]


def coordenada_coluna(coord):
    """ |coordenada_coluna:  coordenada ----> inteiro
    
|coordenada_linha(coord) recebe uma coordenada como argumennto e devolve
um inteiro correspondente 'a coluna dessa coordenada.
    """    
    return coord[1]


# RECONHECEDOR:
def e_coordenada(arg):
    """ |e_coordenada: universal ----> logico
    
|e_coordenada(arg) recebe um argumento e devolve True se este for uma
 coordenada e False caso contrario.
     """    
    return isinstance(arg, tuple) and len(arg)==2 and int_pos(arg[0]) and\
int_pos(arg[1])


# TESTES:
def coordenadas_iguais(coord1, coord2):
    """ |coordenadas_iguais: coordenada x coordenada ----> logico
    
|coordenadas_iguais recebe duas coordenas como argumentos e devolve
True se estas forem iguais e False caso contrario.
       """       
    return coord1 == coord2


# REPRESENTACAO EXTERNA:
def coordenada_para_cadeia(coord):
    """ |coordenada_para_cadeia: coordenada ----> cad. caracteres
    
|coordenada_para_cadeia recebe uma coordenada como argumento e devolve
uma string com os valores da coordenada no formato (l : c).
       """       
    return ("(" + str(coord[0]) + ' : ' + str(coord[1]) + ")")
#---------------------------------------------------------------

# TABULEIRO
# AUXILIARES
def e_coordenada_do_tabuleiro(tabuleiro, coord):
    """ |e_coordenada_do_tabuleiro: tabuleiro x coordenada ----> logico
    
|e_coordenada_do_tabuleiro(tabuleiro, coord) recebe um tabuleiro e uma 
coordenada como argumentos e verifica se a coordenada pertence ao 
tabuleiro, em caso positivo devolve True e em caso contrario False.
       """         
    dim = tabuleiro_dimensoes(tabuleiro)
    lin = coordenada_linha(coord)
    col = coordenada_coluna(coord)
    return e_coordenada(coord) and lin <= dim[0] and col <= dim[1]


def jogo_valido(tuplo):
    """ |jogo_valido: tuplo ----> logico
    
|jogo_valido(tuplo) recebe um tuplo como argumento com as 
especificacoes de um de um tabuleiro e verifica se 'e possivel fazer um 
jogo com essas, caso seja devolve o valor True senao devolve False.
       """        
    colunas_int = 0
    linhas_int = 0
    for el in tuplo[0]:
        for i in range(len(el)):
            colunas_int = colunas_int + el[i]
    for el in tuplo[1]:
        for i in range(len(el)):
            linhas_int = linhas_int + el[i]
    #Para que um jogo seja possivel de se resolver a soma de todos os elementos
    #da especificacoes das colunas deve ser igual 'a soma de todos os elementos
    #das especificoes das linhas.
    return linhas_int == colunas_int
                                  
                                                                                            
def verifica_argumentos(arg):
    """ |verifica_argumentos: universal ----> logico
    
|verifica_argumentos(arg) recebe um argumento e verifica se este e' 
tuplo composto por tuplos compostos por inteiros. Caso tudo se 
verifique, devolve True, no caso de uma das verificacoes nao se 
confirmar devolve False.
          """      
    if isinstance(arg, tuple):
        for el in arg:
            if isinstance(el, tuple):
                for j in el:
                    if not isinstance(j, int) and j < 1:
                        return False
            else:
                return False
    else:
        return False
    return True


def comp_max(tuplo):
    """ |comp_max: tuplo ----> inteiro
    
|comp_max(tuplo) recebe um tuplo composto por tuplos compostos por 
inteiros e devolve o comprimento do maior tuplo do tuplo argumento.
    """
    max_comp = 0
    for i in tuplo:
        if len(i) > max_comp:
            max_comp = len(i)
    return max_comp


# CONSTRUTORES:
def cria_tabuleiro(tuplo):
    """ |cria_tabuleiro: tuplo ----> tabuleiro
    
|cria_tabuleiro(tuplo) recebe um tuplo com as especificacoes do tabuleiro
e devolve um tabuleiro.
    """
    if isinstance(tuplo, tuple) and len(tuplo) == 2 and len(tuplo[0]) == \
       len(tuplo[1]) and verifica_argumentos(tuplo[0]) and \
       verifica_argumentos(tuplo[1]) and jogo_valido(tuplo):
        comp = len(tuplo[0])
        lst_aux = []
        for i in range(comp):
            lst_aux = lst_aux + [[0]*comp]            
        return [tuplo[0], tuplo[1]] + lst_aux
    else:
        raise ValueError('cria_tabuleiro: argumentos invalidos')  


# SELETORES:
def tabuleiro_dimensoes(tabuleiro):
    """ |tabuleiro_dimensoes: tabuleiro ----> tuplo
    
|tabuleiro_dimensoes(tabuleiro) recebe um tabuleiro e devolve um tuplo com \
as dimensoes do tabuleiro.
    """    
    return (len(tabuleiro[0]), len(tabuleiro[1]))

def tabuleiro_especificacoes(tabuleiro):
    """ |tabuleiro_especificacoes: tabuleiro ----> tuplo
    
|tabuleiro_especificacoes(tabuleiro) recebe um tabuleiro e devolve um \
tuplo com as especificacoes do tabuleiro.
    """        
    return (tabuleiro[0], tabuleiro[1])

def tabuleiro_celula(tabuleiro, coord):
    """ |tabuleiro_celula: tabuleiro x coordenada ----> {0, 1, 2}
    
|tabuleiro_celula(tabuleiro, coord) recebe um tabuleiro e uma coordenada e \
devolve o valor contido na coordenada do tabuleiro.
    """            
    if e_coordenada_do_tabuleiro(tabuleiro, coord):
        lin = coordenada_linha(coord)
        col = coordenada_coluna(coord)
        #lin+1 pois os primeiros dois indices sao ocupados pelas especificacoes
        #col-1 pois os indices comecam em 0
        return tabuleiro[lin+1][col-1] 
    else:
        raise ValueError('tabuleiro_celula: argumentos invalidos')


#MODIFICADORES:
def tabuleiro_preenche_celula(tabuleiro, coord, inteiro):
    """ |tabuleiro_preenche_celula: tabuleiro x coordenada x {0, 1, 2} ----> \
tabuleiro
    
|tabuleiro_preenche_celula(tabuleiro, coord, inteiro) recebe um tabuleiro, 
uma coordenada e um valor de {0, 1, 2} e caso os argumentos sejam validos 
devolve um tabuleiro atualizado com o valor(inteiro) na coordenada 
fornecida. Senao devolve a mensagem de erro 'tabuleiro_preenche_celula: 
argumentos invalidos'.
    """         
    if e_coordenada_do_tabuleiro(tabuleiro, coord) and inteiro in [0,1,2]:
        lin = coordenada_linha(coord)
        col = coordenada_coluna(coord)        
        tabuleiro[lin+1][col-1] = inteiro
        return tabuleiro
    else:
        raise ValueError('tabuleiro_preenche_celula: argumentos invalidos')
    
    
#RECONHCEDORES:
def e_tabuleiro(arg):
    """ |e_tabuleiro: universal ----> logico
    
|e_tabuleiro(arg) recebe um argumento e devolve True se este for do tipo
tabuleiro e False em caso contrario.
    """             
    if verifica_argumentos(arg[0]) and verifica_argumentos(arg[1]) and\
    (len(arg[0]) == len(arg[1])) and len(arg) == len(arg[0]) + 2 and\
    jogo_valido((arg[0], arg[1])):
        comp = len(arg)
        for i in range(3,comp):
            if isinstance(arg[i], list):
                for j in arg[i]:
                    if j not in [0,1,2]:
                        return False
            else:
                return False
        return True
    else:
        return False
    
    
def tabuleiro_completo(tabuleiro):
    """ |tabuleiros_completo: tabuleiro ----> logico
    
|tabuleiro_completo(tabuleiro) recebe um tabuleiro e faz a verificacao se
este esta bem preenchido de acordo com as especificacoes, em caso positivo 
devolve True e em caso negativo devolve False.
"""
    #Verifica se a soma de todos os elementos da especificacao da coluna 'e 
    #igual ao numero de '2' em cada coluna.
    for col in range(len(tabuleiro[1])):
        soma = 0
        for i in tabuleiro[1][col]:
            soma = i + soma
        soma_de_colunas = 0
        for i in range(2, len(tabuleiro)):
            if tabuleiro[i][col] == 0:
                return False
            if tabuleiro[i][col] == 2:
                soma_de_colunas = soma_de_colunas + tabuleiro[i][col]
        if soma_de_colunas//2 != soma:
            return False
    #Verifica se a soma de todos os elementos da especificacao da linha 'e igual
    #ao numero de '2' em cada linha.        
    for lin in range(len(tabuleiro[0])):
        soma = 0
        for i in tabuleiro[0][lin]:
            soma = soma + i
        soma_de_linhas = 0
        for i in tabuleiro[lin+2]:
            if i == 0:
                return False
            elif i == 2:
                soma_de_linhas = soma_de_linhas + i
        if soma_de_linhas//2 != soma:
            return False
    return True
    
    
#TESTES:
def tabuleiros_iguais(tab1, tab2):
    """ |tabuleiros_iguais: tabuleiro x tabuleiro ----> logico
    
|tabuleiros_iguais(tab1, tab2) recebe dois tabuleiros como argumentos 
e devolve True se estes forem iguais e False em caso contrario.
    """        
    return tab1 == tab2
        
        
#TRANSFORMADOR DE SAIDA:
def escreve_tabuleiro(tabuleiro):
    """ |escreve_tabuleiro: tabuleiro ----> {} 
    
|escreve_tabuleiro(tabuleiro) recebe um tabuleiro como argumento e escreve no 
ecra a sua representacao externa. Em caso do tabuleiro seja invalido devolve a 
mensagem de erro 'escreve_tabuleiro: argumenos invalidos'.
    """           
    if e_tabuleiro(tabuleiro):
        specs = tabuleiro_especificacoes(tabuleiro)
        max_comp = comp_max(specs[1])
        #Ciclo que escreve as especificacoes das colunas
        while max_comp > 0:
            col = '  '
            for indice in range(len(specs[1])):
                if len(specs[1][indice]) < max_comp:
                    col = col + '     '
                else:
                    col = col + str(specs[1][indice][-max_comp]) + '    '         
            max_comp = max_comp - 1
            print(col)
        max_comp = comp_max(specs[0])
        dim = tabuleiro_dimensoes(tabuleiro)
        elementos = ['?','.','x']
        #Este ciclo e' responsavel por escrever as linhas do tabuleiro, 
        #juntamente com a sua especificacao.
        for lin in range(dim[0]):
            escr = ''
            for col in range(dim[1]):
                    coord = cria_coordenada(lin+1, col+1)
                    valor = tabuleiro_celula(tabuleiro, coord)
                    escr = escr + '[ ' + elementos[valor] + ' ]' 
            limite = max_comp
            for esp in specs[0][lin]:
                escr = escr + ' ' + str(esp)
                limite = limite - 1
            escr = escr + '  '*limite + '|'
            print(escr)
        print()
    else:
        raise ValueError('escreve_tabuleiro: argumenos invalidos')
                        

#-------------------------------------------------------
# JOGADA
def cria_jogada(coord, inteiro):
    """ |cria_jogada: coord x {1, 2} ----> jogada
    
|cria_jogada(coord, inteiro) recebe uma coordenada e um valor(inteiro) e caso os
argumentos sejam validos devolve uma jogada, caso contrario devolve a mensagem
de erro 'cria_jogada: argumentos invalidos'.
    """      
    if e_coordenada(coord) and inteiro in [1, 2]:
        return (coord, inteiro)
    else:
        raise ValueError('cria_jogada: argumentos invalidos')
    
def jogada_coordenada(jogada):
    """ |jogada_coordenada: jogada ----> coordenada
    
|jogada_coordenada(jogada) recebe uma jogada e devolve a coordenada contida
nessa jogada.
    """      
    return jogada[0]

def jogada_valor(jogada):
    """ |jogada_valor: jogada ----> {1, 2}
    
|jogada_jogada(jogada) recebe uma jogada e devolve valor contido nessa 
jogada.
    """        
    return jogada[1]

def e_jogada(arg):
    """ |e_jogada: universal ----> logico
    
|e_jogada(arg) recebe um argumento e caso este seja uma jogada devolve o valor
True, caso contrario devolve False.
    """           
    return isinstance(arg, tuple) and e_coordenada(arg[0]) and arg[1] in [1,2]

def jogadas_iguais(jog1, jog2):
    """ |jogadas_iguais: jogada x jogada ----> logico
    
|jogadas_iguais(jog1, jog2) recebe duas jogas como argumentos e caso estas sejam
iguais devolve o valor True, caso contrario devolve False.
    """               
    return jog1 == jog2

def jogada_para_cadeia(jogada):
    """ |jogadas_para_cadeia: jogada ----> cad. caracteres
    
|jogada_para_cadeia(jogada) recebe uma jogada e devolve uma string com os
valores da jogada na forma '(x : x) --> x'.
"""
    return '('+str(coordenada_linha(jogada_coordenada(jogada))) + ' : ' + \
           str(coordenada_coluna(jogada_coordenada(jogada))) + ') --> ' + \
           str(jogada_valor(jogada))


#-------------------------------------------------------
# FUNCOES ADICIONAIS
def coordenada_de_string(string):
    """ |coordenada_de_string: cad. caracteres ----> tuplo
    
|coordenada_de_string(string) recebe uma cadeia de caracteres que represente 
uma coordenada e retira os dois valores necessarios e devolve-os num tuplo.
"""
    c = ''
    for i in string:
        if i != ':':
            c = c + i
        else:
            c = c + ','
    return  eval(c)


def celulas_vazias(tabuleiro):
    """ |celulas_vazias: tabuleiro ----> lista
    
|celulas_vazias(tabuleiro) recebe um tabuleiro e devolve uma lista com as
coordenas das celulas que estao vazias.
"""    
    cel_vaz = []
    dim = tabuleiro_dimensoes(tabuleiro)
    for lin in range(dim[0]):
        for col in range(dim[1]):
            coord = cria_coordenada(lin+1, col+1)
            if tabuleiro_celula(tabuleiro, coord) == 0:
                cel_vaz = cel_vaz + [coord]
    return cel_vaz
    
    
def le_tabuleiro(nome_do_ficheiro):
    """ |le_tabuleiro: cad. caracteres ----> tuplo
    
|le_tabuleiro(nome_do_ficheiro) recebe uma cadeia de carcateres que corresponde
ao nome de um ficheiro com os dados de especificacao do jogo e devolve um tuplo
com a especificacao das linhas e colunas, respetivamente.
"""        
    fich = open(nome_do_ficheiro, 'r')
    texto = fich.readline()
    fich.close()
    return eval(texto)


def pede_jogada(tabuleiro):
    """ |pede_jogada: tabuleiro ----> jogada
    
|pede_jogada(tabuleiro) recebe um tabuleiro como argumento e devolve a jogada 
que o jogador pretende executar. A funcao pegunta a coordenada e o valor 
pretendidos para a jogada e caso a coordenada nao seja valida a funcao devolve
o valor False.
"""            
    tab_dim = tabuleiro_dimensoes(tabuleiro)
    c = input('Introduza a jogada\n- coordenada entre (1 : 1) e (' + \
              str(tab_dim[0]) + ' : ' + str(tab_dim[1]) + ') >> ')
    v = eval(input('- valor >> '))
    coord = coordenada_de_string(c)
    c1 = cria_coordenada(coord[0], coord[1])
    if coordenada_linha(c1) <= tab_dim[0] and coordenada_coluna(c1) <= tab_dim[1]:
        return cria_jogada(c1, v)
    else:
        return False


def jogo_picross(nome_do_ficheiro):
    """ |jogo_picross: cad. caracteres ----> logico
    
|jogo_picross(nome_do_ficheiro) recebe uma cadeia de carcateres que corresponde
ao nome de um ficheiro com os dados de especificacao do jogo e devolve True caso
o tabuleiro esteja completo (corretamente) e False caso contrario.
"""                
    tab = cria_tabuleiro(le_tabuleiro(nome_do_ficheiro))   
    print('JOGO PICROSS')
    cel_vazias = celulas_vazias(tab)
    escreve_tabuleiro(tab)
    while len(cel_vazias) > 0:
        joga = pede_jogada(tab)
        if joga == False:
            print('Jogada invalida.')
        else:
            tabuleiro_preenche_celula(tab, jogada_coordenada(joga), jogada_valor(joga))
            escreve_tabuleiro(tab)
        cel_vazias = celulas_vazias(tab)
    if tabuleiro_completo(tab):
        print('JOGO: Parabens, encontrou a solucao!')
        return True
    else:
        print('JOGO: O tabuleiro nao esta correto!')
        return False
        
